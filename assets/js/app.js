import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import TodoContextProvider from "./contexts/TodoContext";
import TaskList from "./components/TaskList";

class App extends Component {
    render() {
        return (
            <TodoContextProvider>
                <TaskList/>
            </TodoContextProvider>
        );
    }
}

ReactDOM.render(<App/>, document.getElementById('root'))