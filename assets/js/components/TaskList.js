import React, {Component, useContext} from 'react';
import {TodoContext} from "../contexts/TodoContext";

function TaskList() {
    const context = useContext(TodoContext)
    return (
        <ul>
            {context.tasks.map(task => (
                <li>{task.title}</li>
            ))}
        </ul>
    )
}

export default TaskList;