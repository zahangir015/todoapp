import React, {Component, createContext} from 'react'

export const TodoContext = createContext()

class TodoContextProvider extends Component {
    constructor(props) {
        super(props)
        this.state = {
            tasks: [
                {title: 'Task1'},
                {title: 'Task2'},
                {title: 'Task3'},
            ]
        }
    }

    //Todo CRUD operations
    create = () => {

    }

    read = () => {

    }

    update = () => {

    }

    delete = () => {

    }


    render() {
        return (
            <TodoContext.Provider value={{
                ...this.state,
                create: this.create.bind(this),
                update: this.update.bind(this),
                delete: this.delete.bind(this),
            }}>
                {this.props.children}
            </TodoContext.Provider>
        );
    }
}

export default TodoContextProvider;